$('#draggable-move').draggable({
cursor: "move"
});

$('#draggable-pointer').draggable({
cursor: "pointer",
cursorAt: { top: -5, left: -5 }
});

$('#draggable-crosshair').draggable({
cursor: "crosshair",
cursorAt: { bottom: 0 }
});
