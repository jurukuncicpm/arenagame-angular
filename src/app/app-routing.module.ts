import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SignComponent } from './components/sign/sign.component';
import { SignupComponent } from './components/signup/signup.component';
import { TeamallComponent } from './components/board/teamall/teamall.component';


const routes: Routes = [
  {path:'', component:HomeComponent},
  {path:'sign', component:SignComponent},
  {path:'signup', component:SignupComponent},
  {path:'team', component:TeamallComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
