import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { User } from '../../models/user';

import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
    headers = new HttpHeaders().set('Content-Type', 'application/json');
    datauser = {}
  constructor(
      private http:HttpClient,
      private router: Router,
      ) { }
      signIn(user: User){
          return this.http.post<any>(`${environment.urlAddress}signin`, user).subscribe(
              complete =>{
                localStorage.setItem('access_token',complete.token)

                     this.getUserProfile(complete.user._id).subscribe((res:any)=>{
                         this.datauser = res.user;
                         this.router.navigate(['user/profile-user/'+res._id])
                     })
              }
            )

      }
      logout(){
          let hapusToken = localStorage.removeItem('access_token');
          if(hapusToken==null){
              this.router.navigate([''])
          }
      }

      get isLogin(): boolean{
          let token = localStorage.getItem('access_token');
          return (token !==null) ? true : false
      }

      getToken(){
          return localStorage.getItem('access_token');
      }

      getUserProfile(_id): Observable<any>{
          let api = `${environment.urlAddress}user/${_id}`;
          return this.http.get(api,{
              headers: this.headers
          }).pipe(
              map((res: Response)=>{
                  return res || {}
              }),
              catchError(this.handleError)
          )
      }

      handleError(error:HttpErrorResponse){
            let pesan = '';

            if(error.error instanceof ErrorEvent){
                pesan =error.error.message

            }else{
                pesan = `Error code: ${error.status} \n Pesan Error: ${error.message}`;
            }
            return throwError(pesan);

      }
      signUp(user: User){
        return this.http.post<any>(`${environment.urlAddress}signup`, user).subscribe((res:any)=>{
            alert("register berhasil")
            this.router.navigate(['user/signin'])

        })
    }

}
