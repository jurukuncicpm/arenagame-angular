import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router';
import { from } from 'rxjs';
import { AuthService } from '../../services/auth/auth.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  form : any ;
  signUpForm:FormGroup
  constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    public router: Router) {
      this.signUpForm = this.fb.group({
        username:[''],
        email:[''],
        password:[''],
        namadepan:[''],
        namabelakang:[''],
        umur:null
      })
     }

  ngOnInit(): void {

  }

  signUp(){
    this.form  = this.signUpForm.value;

    if(this.form.role === undefined){
        this.form.role = 0
        this.authService.signUp(this.form)
        // console.log(this.form.role);
    }

  }
}
